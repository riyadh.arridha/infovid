import React, { Component } from "react";
import {
  WelcomePage,
  LoginPage,
  HomePage,
  CovidPage,
  CegahPage,
  CiriPage,
  VideoPage,
  UpdatePage,
  AboutPage,
  SplashPage,
} from "./src/pages";
import Global from "./src/pages/Update/globalDetail";

import Router from "./src/router";
import { NavigationContainer } from "@react-navigation/native";

export default class App extends Component {
  render() {
    // return <SplashPage />;
    // return <WelcomePage />;
    // return <LoginPage />;
    // return <HomePage />;
    // return <CovidPage />;
    // return <CegahPage />;
    // return <CiriPage />;
    // return <VideoPage />;
    // return <UpdatePage />;
    // return <AboutPage />;
    // return <Global />;
    // return <Tes />;
    // return <Router />;
    return (
      <NavigationContainer>
        <Router />
      </NavigationContainer>
    );
  }
}
