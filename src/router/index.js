import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import {
  AboutPage,
  CegahPage,
  CiriPage,
  CovidPage,
  HomePage,
  LoginPage,
  SplashPage,
  UpdatePage,
  VideoPage,
  WelcomePage,
} from "../pages";
import GlobalPage from "./../pages/Update/globalDetail";
import IndoPage from "./../pages/Update/indoDetail";

const Stack = createStackNavigator();
// const Tab = createBottomTabNavigator();
// const Drawer = createDrawerNavigator();

// function DrawerScreen() {
//   return (
//     <Drawer.Navigator>
//       <Drawer.Screen name="Skill" component={TabScreen} />
//       <Drawer.Screen name="About" component={About} />
//       {/* <Drawer.Screen name="Notifications" component={NotificationsScreen} /> */}
//     </Drawer.Navigator>
//   );
// }

// function TabScreen() {
//   return (
//     <Tab.Navigator>
//       <Tab.Screen name="Skills" component={Skills} />
//       <Tab.Screen name="Projects" component={Project} />
//       <Tab.Screen name="Add" component={Add} />
//     </Tab.Navigator>
//   );
// }

const Router = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Splash"
        component={SplashPage}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Welcome"
        component={WelcomePage}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Login"
        component={LoginPage}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Home"
        component={HomePage}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Covid"
        component={CovidPage}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Ciri"
        component={CiriPage}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Cegah"
        component={CegahPage}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Video"
        component={VideoPage}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Update"
        component={UpdatePage}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Global"
        component={GlobalPage}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Indo"
        component={IndoPage}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="About"
        component={AboutPage}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

export default Router;
