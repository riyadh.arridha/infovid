import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  Linking,
} from "react-native";
import { colors } from "./../../utils/colors";
import { fonts } from "./../../utils/fonts";
import { Welcome } from "./../../assets";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { StatusBar } from "expo-status-bar";

const DEVICE = Dimensions.get("window");

export default class VideoPage extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        {/* <StatusBar /> */}

        <View style={styles.top}>
          <View style={styles.buttonBack}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}
            >
              <Icon name="chevron-left" size={30} color={colors.text.dark} />
            </TouchableOpacity>
          </View>

          <Text style={styles.hi}>Hi, User</Text>
          <Text style={styles.textHeader}>
            Berikut video kampanye pencegahan Covid-19
          </Text>
        </View>

        <SafeAreaView style={styles.bottom}>
          <ScrollView>
            <Text style={styles.paragraph}>LIST VIDEO</Text>
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: "center",
    // alignItems: "center",
    backgroundColor: colors.default,
  },
  top: {
    backgroundColor: colors.default,
    height: "20%",
    width: "100%",
  },
  buttonBack: {
    marginTop: 25,
    marginLeft: 15,
  },
  hi: {
    fontSize: 24,
    fontFamily: fonts.default,
    color: "transparent",
    fontWeight: "bold",
    marginLeft: 15,
  },
  textHeader: {
    fontSize: 14,
    fontWeight: "bold",
    fontFamily: fonts.default,
    color: colors.text.dark,
    // textAlign: "center",
    marginHorizontal: 15,
  },
  bottom: {
    // height: "25%",
    flex: 1,
    width: "100%",
    marginBottom: 0,
    backgroundColor: "#FFF",
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    paddingTop: 20,
    // alignItems: "center",
  },
  paragraph: {
    fontSize: 16,
    fontWeight: "normal",
    fontFamily: fonts.default,
    color: colors.text.dark,
    textAlign: "justify",
    margin: 25,
    lineHeight: 20,
  },
  next: {
    textAlign: "right",
    marginRight: 20,
  },
});
