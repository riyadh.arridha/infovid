import React, { useEffect } from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import { LogoApp } from "./../../assets";
import { fonts } from "./../../utils/fonts";
import { colors } from "./../../utils/colors";

const SplashPage = ({ navigation }) => {
  //ini adalah props bawaan dari navigation container
  useEffect(() => {
    //sama dengan ComponentDidMount di class component
    setTimeout(() => {
      navigation.replace("Welcome"); //.navigate 'back' berfungsi
    }, 2000);
  });
  return (
    <View style={styles.container}>
      <Image source={LogoApp} style={styles.logo}></Image>
      <Text style={styles.textApp}>INFOVID19</Text>
      <Text style={styles.textSubApp}>MOBILE APP</Text>
    </View>
  );
};

export default SplashPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.default,
  },
  logo: {
    height: 90,
    width: 90,
    marginBottom: 20,
  },
  textApp: {
    fontSize: 36,
    fontWeight: "bold",
    fontFamily: fonts.default,
    color: colors.text.dark,
  },
  textSubApp: {
    fontSize: 18,
    fontWeight: "bold",
    fontFamily: fonts.default,
    color: colors.text.dark,
  },
});
