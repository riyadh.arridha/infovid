import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { Photo, Facebook, Instagram, Twitter, Gitlab } from "./../../assets";
import { colors } from "./../../utils/colors";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Sosmed from "./sosmed";

export default class AboutPage extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <ScrollView>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
          >
            <View style={styles.header}>
              <Icon name="chevron-left" size={30} color={colors.text.dark} />
            </View>
          </TouchableOpacity>
          <View style={styles.top}>
            <Image source={Photo} style={{ height: 121, width: 89 }}></Image>
            <Text style={styles.nameText}>Riyadh Arridha</Text>
          </View>

          <View style={styles.bottom}>
            <View style={styles.bottomView}>
              <Sosmed
                images={Facebook}
                texts="@riyadh.arridha"
                links="https://www.facebook.com/riyadh.arridha/"
              />
              <Sosmed
                images={Instagram}
                texts="@riyadh.arridha"
                links="https://www.instagram.com/riyadh.arridha/"
              />
              <Sosmed
                images={Twitter}
                texts="@riyadh_arridha"
                links="https://www.twitter.com/riyadh_arridha/"
              />
              <Text style={styles.caption}>Social Media Account</Text>
              <Sosmed
                images={Gitlab}
                texts="gitlab/@riyadh.arridha"
                links="https://www.gitlab.com/riyadh.arridha/"
              />
              <Text style={styles.caption}>My Awesome Project</Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.default,
  },

  //tombol back
  header: {
    height: "5%",
    marginTop: 25,
    marginLeft: 15,
  },

  //atas
  top: {
    height: "25%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.default,
  },
  nameText: {
    fontSize: 24,
    color: colors.text.dark,
    fontWeight: "bold",
    margin: 10,
    marginBottom: 20,
  },

  //bawah
  bottom: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderTopStartRadius: 50,
    borderTopEndRadius: 50,
    backgroundColor: "#FFF",
  },
  caption: {
    fontSize: 18,
    color: colors.default,
    fontWeight: "bold",
    marginBottom: 30,
    margin: 10,
  },
  bottomView: {
    margin: 30,
    marginTop: 40,
  },
});
