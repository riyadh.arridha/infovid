import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Linking,
} from "react-native";

import { colors } from "./../../utils/colors";

export default class Sosmed extends Component {
  render() {
    return (
      <TouchableOpacity onPress={() => Linking.openURL(this.props.links)}>
        <View style={styles.sosmedView}>
          <Image
            source={this.props.images}
            style={{ height: 30, width: 30 }}
          ></Image>
          <Text style={styles.bottomText}>{this.props.texts}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  sosmedView: {
    alignItems: "center",
  },
  bottomText: {
    fontSize: 14,
    fontWeight: "bold",
    color: colors.text.minDark,
    margin: 10,
  },
});
