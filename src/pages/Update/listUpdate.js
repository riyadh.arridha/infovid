import * as React from "react";
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  Platform,
} from "react-native";
import { SearchBar } from "react-native-elements";
import CustomListItem from "./customListItem";
import { colors } from "./../../utils/colors";
import Infos from "./../Home/infos";

export default class ListUpdate extends Component {
  render() {
    return (
      <View style={styles.infoView}>
        <Text style={styles.infoTitle}>{this.props.code}</Text>
        <View style={styles.infoContainer}>
          <Infos
            title="Positif"
            content={this.props.positif.value}
            color="orange"
          />
          <Infos
            title="Sembuh"
            content={this.props.sembuh.value}
            color="green"
          />
          <Infos
            title="Meninggal"
            content={this.props.meninggal.value}
            color="red"
          />
        </View>
        <TouchableOpacity
          onPress={() => {
            handleGoTo("Global");
          }}
        >
          <Text style={styles.nextText}>Selengkapnya...</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  infoTitle: {
    marginLeft: 20,
    marginTop: 10,
    fontSize: 24,
    fontWeight: "bold",
    color: colors.default,
  },
  infoContainer: {
    flexDirection: "row",
    alignContent: "center",
    // backgroundColor: "blue",
    alignContent: "center",
    justifyContent: "space-around",
    height: 120,
    width: "100%",
    // padding: 10,
  },
});
