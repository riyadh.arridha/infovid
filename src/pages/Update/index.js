import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  ActivityIndicator,
  Linking,
} from "react-native";
import { colors } from "./../../utils/colors";
import { fonts } from "./../../utils/fonts";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { StatusBar } from "expo-status-bar";
import Infos from "./../Home/infos";
import DataProcessing from "./dataProcessing";

const DEVICE = Dimensions.get("window");

var time = new Date().getTime();
var date = new Date(time);

const urlGlobalPositif = "https://api.kawalcorona.com/positif";
const urlGlobalSembuh = "https://api.kawalcorona.com/sembuh";
const urlGlobalMeninggal = "https://api.kawalcorona.com/meninggal";
const urlIndonesia = "https://api.kawalcorona.com/indonesia";
const urlIndonesiaProvinsi = "https://api.kawalcorona.com/indonesia/provinsi/";

export default class UpdatePage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      url: "",
      positif: "",
      meninggal: "",
      sembuh: "",
      isLoading: true,
    };
  }

  componentDidMount() {
    this.fetchDataGlobal();
    this.fetchDataIndo();
  }

  fetchDataGlobal = async () => {
    try {
      const jsonPositif = await (
        await fetch("https://api.kawalcorona.com/positif")
      ).json();
      const jsonSembuh = await (
        await fetch("https://api.kawalcorona.com/sembuh")
      ).json();
      const jsonMeninggal = await (
        await fetch("https://api.kawalcorona.com/meninggal")
      ).json();
      const jsonGlobal = await (
        await fetch("https://api.kawalcorona.com/")
      ).json();

      if (jsonPositif && jsonSembuh && jsonMeninggal) {
        this.setState({ isLoading: false });
        this.setState({ positif: jsonPositif.value });
        this.setState({ sembuh: jsonSembuh.value });
        this.setState({ meninggal: jsonMeninggal.value });
      }
    } catch (err) {
      alert(err);
    }
  };

  fetchDataIndo = async () => {
    try {
      const jsonData = await (
        await fetch("https://api.kawalcorona.com/indonesia")
      ).json();

      if (jsonData) {
        this.setState({ isLoading: false });
        this.setState({ positif1: jsonData[0].positif });
        this.setState({ sembuh1: jsonData[0].sembuh });
        this.setState({ meninggal1: jsonData[0].meninggal });
        // console.log(jsonData);
        // console.log(jsonData[0].meninggal1);
      }
    } catch (err) {
      alert(err);
    }
  };

  render() {
    const { navigation } = this.props;
    const handleGoTo = (screen) => {
      // const { navigation } = this.props;
      // console.log(screen);
      navigation.navigate(screen);
    };
    if (this.state.isLoading) {
      //Loading View while data is loading
      return (
        <View style={{ flex: 1, paddingTop: 20 }}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        {/* <DataProcessing
          url={this.state.url}
          sembuh={this.state.sembuh}
          positif={this.state.positif}
          meninggal={this.state.meninggal}
        /> */}
        {/* <StatusBar /> */}
        <View style={styles.top}>
          <View style={styles.buttonBack}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}
            >
              <Icon name="chevron-left" size={30} color={colors.text.dark} />
            </TouchableOpacity>
          </View>

          <Text style={styles.hi}>Hi, User</Text>
          <Text style={styles.textHeader}>
            Berikut update informasi penderita Covid-19
          </Text>
          <Text style={styles.lastUpdate}>Last Update ({date.toString()})</Text>
        </View>
        <SafeAreaView style={styles.bottom}>
          <ScrollView>
            {/* <this.tampilan
              infoTitle="Global"
              positif={this.state.positif}
              sembuh={this.state.sembuh}
              meninggal={this.state.meninggal}
            /> */}
            <View style={styles.infoView}>
              <Text style={styles.infoTitle}>Dunia</Text>
              <View style={styles.infoContainer}>
                <Infos
                  title="Positif"
                  content={this.state.positif}
                  color="orange"
                />
                <Infos
                  title="Sembuh"
                  content={this.state.sembuh}
                  color="green"
                />
                <Infos
                  title="Meninggal"
                  content={this.state.meninggal}
                  color="red"
                />
              </View>
              <TouchableOpacity
                onPress={() => {
                  handleGoTo("Global");
                }}
              >
                <Text style={styles.nextText}>Selengkapnya...</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.infoView}>
              <Text style={styles.infoTitle}>Indonesia</Text>
              <View style={styles.infoContainer}>
                <Infos
                  title="Positif"
                  content={this.state.positif1}
                  color="orange"
                />
                <Infos
                  title="Sembuh"
                  content={this.state.sembuh1}
                  color="green"
                />
                <Infos
                  title="Meninggal"
                  content={this.state.meninggal1}
                  color="red"
                />
              </View>
              <TouchableOpacity
                onPress={() => {
                  handleGoTo("Indo");
                }}
              >
                <Text style={styles.nextText}>Selengkapnya...</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: "center",
    // alignItems: "center",
    backgroundColor: colors.default,
  },
  top: {
    backgroundColor: colors.default,
    height: "20%",
    width: "100%",
  },
  buttonBack: {
    marginTop: 25,
    marginLeft: 15,
  },
  hi: {
    fontSize: 24,
    fontFamily: fonts.default,
    color: "transparent",
    fontWeight: "bold",
    marginLeft: 15,
  },
  textHeader: {
    fontSize: 14,
    fontWeight: "bold",
    fontFamily: fonts.default,
    color: colors.text.dark,
    // textAlign: "center",
    marginHorizontal: 15,
  },
  lastUpdate: {
    color: colors.text.minDark,
    marginHorizontal: 15,
  },
  infoView: {
    backgroundColor: "white",
    borderTopRightRadius: 50,
    borderBottomRightRadius: 50,
    marginVertical: 5,
  },
  infoTitle: {
    marginLeft: 20,
    marginTop: 10,
    fontSize: 24,
    fontWeight: "bold",
    color: colors.default,
  },
  infoContainer: {
    flexDirection: "row",
    alignContent: "center",
    // backgroundColor: "blue",
    alignContent: "center",
    justifyContent: "space-around",
    height: 120,
    width: "100%",
    // padding: 10,
  },
  nextText: {
    // marginLeft: 20,
    // marginTop: 10,
    fontSize: 16,
    marginRight: 30,
    textAlign: "right",
    fontWeight: "bold",
    color: colors.default,
    marginBottom: 15,
  },
  bottom: {
    // height: 160,
    flex: 1,
    width: "100%",
    marginBottom: 0,
    backgroundColor: colors.default,
    // paddingTop: 20,
    // alignItems: "center",
  },
});
