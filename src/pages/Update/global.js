import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  ActivityIndicator,
  Linking,
} from "react-native";
import { colors } from "./../../utils/colors";
import { fonts } from "./../../utils/fonts";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { StatusBar } from "expo-status-bar";
import Infos from "./../Home/infos";

const DEVICE = Dimensions.get("window");

export default class Global extends Component {
  state = {
    url: "",
    positif: "https://api.kawalcorona.com/positif",
    meninggal: "https://api.kawalcorona.com/meninggal",
    sembuh: "https://api.kawalcorona.com/sembuh",
    isLoading: true,
    code: "",
  };
  componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    try {
      const jsonPositif = await (
        await fetch("https://api.kawalcorona.com/positif")
      ).json();
      const jsonSembuh = await (
        await fetch("https://api.kawalcorona.com/sembuh")
      ).json();
      const jsonMeninggal = await (
        await fetch("https://api.kawalcorona.com/meninggal")
      ).json();
      if (jsonPositif && jsonSembuh && jsonMeninggal) {
        this.setState({ isLoading: false });
        this.setState({ positif: jsonPositif });
        this.setState({ sembuh: jsonSembuh });
        this.setState({ meninggal: jsonMeninggal });
      }
    } catch (err) {
      alert(err);
    }

    // console.log(json);
  };

  tampilan() {}

  render() {
    const { navigation } = this.props;
    const handleGoTo = (screen) => {
      navigation.navigate(screen);
    };
    if (this.state.isLoading) {
      //Loading View while data is loading
      return (
        <View style={{ flex: 1, paddingTop: 20 }}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        {/* <StatusBar /> */}

        <View style={styles.top}>
          <View style={styles.buttonBack}>
            <TouchableOpacity>
              <Icon name="chevron-left" size={30} color={colors.text.dark} />
            </TouchableOpacity>
          </View>

          <Text style={styles.hi}>Hi, User</Text>
          <Text style={styles.textHeader}>
            Berikut update informasi penderita Covid-19
          </Text>
          <Text style={styles.lastUpdate}>Last update (07 Juni 2020)</Text>
        </View>

        <SafeAreaView style={styles.bottom}>
          <ScrollView>
            <View style={styles.infoView}>
              <Text style={styles.infoTitle}>Dunia</Text>
              <View style={styles.infoContainer}>
                <Infos
                  title="Positif"
                  content={this.state.positif.value}
                  color="orange"
                />
                <Infos
                  title="Sembuh"
                  content={this.state.sembuh.value}
                  color="green"
                />
                <Infos
                  title="Meninggal"
                  content={this.state.meninggal.value}
                  color="red"
                />
              </View>
              <TouchableOpacity
                onPress={() => {
                  handleGoTo("Global");
                }}
              >
                <Text style={styles.nextText}>Selengkapnya...</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.infoView}>
              <Text style={styles.infoTitle}>Dunia</Text>
              <View style={styles.infoContainer}>
                <Infos title="Positif" content="170.977" color="orange" />
                <Infos title="Sembuh" content="301.251" color="green" />
                <Infos title="Meninggal" content="8.702" color="red" />
              </View>
            </View>
            <View style={styles.infoView}>
              <Text style={styles.infoTitle}>Dunia</Text>
              <View style={styles.infoContainer}>
                <Infos title="Positif" content="170.977" color="orange" />
                <Infos title="Sembuh" content="301.251" color="green" />
                <Infos title="Meninggal" content="8.702" color="red" />
              </View>
            </View>
            <View style={styles.infoView}>
              <Text style={styles.infoTitle}>Dunia</Text>
              <View style={styles.infoContainer}>
                <Infos title="Positif" content="170.977" color="orange" />
                <Infos title="Sembuh" content="301.251" color="green" />
                <Infos title="Meninggal" content="8.702" color="red" />
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: "center",
    // alignItems: "center",
    backgroundColor: colors.default,
  },
  top: {
    backgroundColor: colors.default,
    height: "20%",
    width: "100%",
  },
  buttonBack: {
    marginTop: 25,
    marginLeft: 15,
  },
  hi: {
    fontSize: 24,
    fontFamily: fonts.default,
    color: colors.text.dark,
    fontWeight: "bold",
    marginLeft: 15,
  },
  textHeader: {
    fontSize: 14,
    fontWeight: "bold",
    fontFamily: fonts.default,
    color: colors.text.dark,
    // textAlign: "center",
    marginHorizontal: 15,
  },
  lastUpdate: {
    color: colors.text.minDark,
    marginHorizontal: 15,
  },
  infoView: {
    backgroundColor: "white",
    borderTopRightRadius: 50,
    borderBottomRightRadius: 50,
    marginVertical: 5,
  },
  infoTitle: {
    marginLeft: 20,
    marginTop: 10,
    fontSize: 24,
    fontWeight: "bold",
    color: colors.default,
  },
  infoContainer: {
    flexDirection: "row",
    alignContent: "center",
    // backgroundColor: "blue",
    alignContent: "center",
    justifyContent: "space-around",
    height: 120,
    width: "100%",
    // padding: 10,
  },
  nextText: {
    // marginLeft: 20,
    // marginTop: 10,
    fontSize: 16,
    marginRight: 30,
    textAlign: "right",
    fontWeight: "bold",
    color: colors.default,
    marginBottom: 15,
  },
  bottom: {
    // height: 160,
    flex: 1,
    width: "100%",
    marginBottom: 0,
    backgroundColor: colors.default,
    // paddingTop: 20,
    // alignItems: "center",
  },
});
