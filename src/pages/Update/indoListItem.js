import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  Linking,
  FlatList,
} from "react-native";
import { colors } from "../../utils/colors";
import { fonts } from "../../utils/fonts";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { StatusBar } from "expo-status-bar";
import Infos from "../Home/infos";
import { ListItem } from "react-native-elements";

export default class CustomListItem extends Component {
  render() {
    let data = this.props.item;
    // console.log(data);
    // let positif = format(data.Confirmed);
    // console.log(positif);
    return (
      <View style={styles.container}>
        <View style={styles.listContainer}>
          <Text style={styles.textTitle}>{data.Provinsi}</Text>
          <View>
            <Text style={styles.textData}>
              Positif: {format(data.Kasus_Posi)} orang
            </Text>
            <Text style={styles.textData}>
              Meninggal: {format(data.Kasus_Meni)} orang
            </Text>
            <Text style={styles.textData}>
              Sembuh: {format(data.Kasus_Semb)} orang
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const format = (num) => {
  const n = String(num),
    p = n.indexOf(".");
  return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, (m, i) =>
    p < 0 || i < p ? `${m},` : m
  );
};

// ;[
//     format(100),                           // "100"
//     format(1000),                          // "1,000"
//     format(1e10),                          // "10,000,000,000"
//     format(1000.001001),                   // "1,000.001001"
//     format('100000000000000.001001001001') // "100,000,000,000,000.001001001001
// ]
//     .forEach(n => console.log(n))

{
  /* // {`Negara:${item.attributes.Country_Region} Positif:${item.attributes.Confirmed} Meninggal:${item.attributes.Deaths}, Sembuh:${item.attributes.Recovered} \n`} */
}
{
  /* // const CustomListItem = ({ item }) => { */
}
{
  /* //   //   console.log(item);
//   //   let country = this.props.negara;
//   console.log(item.Country_Region);
//   return (
//     <View style={{ height: 20, width: "100%" }}>
//       <Text style={{ textAlign: "center" }}>{item.Country_Region}</Text>
//     </View>
//   );
// }; */
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // flexDirection: "row",
    // marginTop: 20,
    // justifyContent: "center",
    // alignItems: "center",
    width: "100%",
    backgroundColor: colors.default,
  },
  listContainer: {
    backgroundColor: "white",
    height: 100,
    width: "100%",
    borderRadius: 20,
    alignItems: "center",
    flexDirection: "row",
    alignContent: "center",
    justifyContent: "space-evenly",
    marginBottom: 5,
    flex: 1,
  },
  textTitle: {
    fontSize: 18,
    fontWeight: "bold",
    color: colors.text.dark,
    textAlign: "center",
  },
  textData: {
    fontSize: 16,
    fontWeight: "bold",
    color: colors.text.minDark,
    textAlign: "center",
  },
});
