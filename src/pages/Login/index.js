import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
} from "react-native";
import { Login } from "./../../assets";
import { colors } from "./../../utils/colors";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

const DEVICE = Dimensions.get("window");

export default class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      password: "",
      isError: false,
    };
  }

  loginHandler() {
    // console.log(this.state.userName, " ", this.state.password);
    if (this.state.password === "infocovid19") {
      this.props.navigation.navigate("Home", {
        userName: this.state.userName,
      });
    } else {
      this.setState({ isError: true });
    }
  }

  render() {
    const { navigation } = this.props;
    // const handleGoTo = (screen) => {
    //   navigation.navigate(screen);
    // };

    return (
      <View style={styles.container}>
        <View style={styles.top}>
          <View style={styles.buttonBack}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}
            >
              <Icon name="chevron-left" size={30} color={colors.text.dark} />
            </TouchableOpacity>
          </View>
          <View style={styles.imagePosition}>
            <Image source={Login} style={styles.loginImage} />
          </View>
        </View>

        <View style={styles.bottom}>
          <Text style={styles.loginCaption}>
            Silahkan Masukkan "Nama Anda" dan masukkan Password "infocovid19"
          </Text>
          <View style={styles.inputBar}>
            <TextInput
              style={styles.inputText}
              placeholder="Your Name"
              onChangeText={(userName) => this.setState({ userName })}
            />
          </View>
          {/* <InputLogin title="Your Name" /> */}
          <View style={styles.inputBar}>
            <TextInput
              style={styles.inputText}
              placeholder="Your Password"
              onChangeText={(password) => this.setState({ password })}
              secureTextEntry={true}
            />
          </View>
          {/* <PasswordLogin title="Password" /> */}
          <Text
            style={
              this.state.isError ? styles.errorText : styles.hiddenErrorText
            }
          >
            Password Salah
          </Text>
          <View style={{ alignItems: "center" }}>
            <TouchableOpacity onPress={() => this.loginHandler()}>
              <View style={styles.button}>
                <Text style={styles.buttonTitle}>MASUK</Text>
              </View>
            </TouchableOpacity>
          </View>

          {/* <View style={{ alignItems: "center", marginTop: 100 }}>
          <Button title="MASUK" onPress={() => navigation.navigate("Skills")} />
        </View> */}
        </View>
      </View>
    );
  }
}

const PasswordLogin = ({ title }) => {
  return (
    <View style={styles.inputBar}>
      <TextInput
        style={styles.inputText}
        placeholder={title}
        onChangeText={(password) => this.setState({ password })}
        secureTextEntry={true}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.default,
  },
  top: {
    height: "40%",
    width: "100%",
    marginTop: 0,
  },
  buttonBack: {
    marginTop: 25,
    marginLeft: 15,
  },
  loginImage: {
    maxWidth: 177,
    maxHeight: 150,
  },
  imagePosition: {
    alignItems: "center",
  },

  bottom: {
    backgroundColor: "white",
    height: "60%",
    width: "100%",
    alignItems: "center",
    borderRadius: 50,
    borderBottomEndRadius: 0,
    borderBottomStartRadius: 0,
    justifyContent: "space-evenly",
  },

  loginCaption: {
    color: colors.text.dark,
    width: "60%",
    fontSize: 14,
    textAlign: "center",
    fontWeight: "bold",
  },

  inputText: {
    fontSize: 14,
    borderBottomWidth: 1,
    borderColor: colors.text.semi,
  },
  inputBar: {
    width: "65%",
    height: 48,
  },

  buttonTitle: {
    textAlign: "center",
    color: "#000",
    fontSize: 14,
    fontWeight: "bold",
  },
  button: {
    backgroundColor: colors.default,
    height: 48,
    width: 232,
    borderRadius: 232 / 2,
    alignItems: "center",
    justifyContent: "center",
  },
  errorText: {
    color: "red",
    textAlign: "center",
    marginBottom: 16,
  },
  hiddenErrorText: {
    color: "transparent",
    textAlign: "center",
    marginBottom: 16,
  },
});
