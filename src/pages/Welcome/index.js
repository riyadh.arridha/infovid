import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import { colors } from "./../../utils/colors";
import { fonts } from "./../../utils/fonts";
import { Welcome } from "../../assets";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { StatusBar } from "expo-status-bar";

const DEVICE = Dimensions.get("window");

export default class WelcomePage extends Component {
  render() {
    const { navigation } = this.props;
    const handleGoTo = (screen) => {
      navigation.navigate(screen);
    };

    return (
      <View style={styles.container}>
        {/* <StatusBar /> */}
        <View style={styles.header}>
          <Image source={Welcome} style={styles.imageSetup} />
          <Text style={styles.texHeader}>
            Mari bersama-sama mencegah penularan Covid-19 dengan mematuhi
            protokol kesehatan yang telah ditetapkan
          </Text>
        </View>
        <View style={styles.footer}>
          <Text style={styles.textFooter}>
            Ingin tahu lebih banyak mengenai Covid-19?
          </Text>
          <TouchableOpacity
            onPress={() => {
              handleGoTo("Login");
            }}
          >
            <Icon
              name="arrow-right"
              color={colors.text.dark}
              size={30}
              style={styles.next}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: "center",
    // alignItems: "center",
    backgroundColor: colors.default,
  },
  header: {
    backgroundColor: "#FFF",
    height: "75%",
    width: "100%",
    marginTop: 0,
    borderBottomEndRadius: 50,
    borderBottomStartRadius: 50,
    justifyContent: "space-around",
    alignItems: "center",
  },
  imageSetup: {
    maxWidth: DEVICE.width * 0.8,
    maxHeight: DEVICE.height * 0.6,
  },
  texHeader: {
    fontSize: 14,
    fontWeight: "bold",
    fontFamily: fonts.default,
    color: colors.text.dark,
    textAlign: "center",
  },
  footer: {
    height: "25%",
    width: "100%",
    marginBottom: 0,
    justifyContent: "space-evenly",
    // alignItems: "center",
  },
  textFooter: {
    fontSize: 18,
    fontWeight: "bold",
    fontFamily: fonts.default,
    color: colors.text.dark,
    textAlign: "center",
    // marginHorizontal: 30,
    paddingHorizontal: 25,
    paddingTop: 10,
  },
  next: {
    textAlign: "right",
    marginRight: 20,
  },
});
