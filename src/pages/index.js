import SplashPage from "./Splash";
import WelcomePage from "./Welcome";
import LoginPage from "./Login";
import HomePage from "./Home";
import CovidPage from "./Covid";
import CegahPage from "./Cegah";
import CiriPage from "./Ciri";
import VideoPage from "./Video";
import UpdatePage from "./Update";
import AboutPage from "./About";

export {
  SplashPage,
  WelcomePage,
  LoginPage,
  HomePage,
  CovidPage,
  CegahPage,
  CiriPage,
  VideoPage,
  UpdatePage,
  AboutPage,
};
