import React, { Component } from "react";
import { StyleSheet, View, Text, Image, TouchableOpacity } from "react-native";
import { colors } from "./../../utils/colors";

export default class Menus extends Component {
  render() {
    return (
        <View style={styles.menuContainer}>
          <View style={styles.menuView}>
            <Image
              source={this.props.images}
              style={{ height: 50, width: 50 }}
            />
          </View>
          <Text style={styles.textMenu}>{this.props.names}</Text>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  menuContainer: {
    // backgroundColor: "red",
    height: 100,
    width: 80,
    marginHorizontal: 40,
    marginVertical: 15,
  },
  menuView: {
    height: 80,
    width: 80,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 20,
    backgroundColor: colors.default,
  },
  textMenu: {
    fontSize: 14,
    fontWeight: "bold",
    textAlign: "center",
    fontFamily: "Roboto",
  },
});
