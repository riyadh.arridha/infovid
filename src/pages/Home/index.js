import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import { colors } from "./../../utils/colors";
import { fonts } from "./../../utils/fonts";
import { Covid, Cegah, Ciri, Video, Info, About } from "./../../assets";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Menus from "./menus";
import Infos from "./infos";

const DEVICE = Dimensions.get("window");

export default class HomePage extends Component {
  state = {
    url: "",
    positif: "",
    meninggal: "",
    sembuh: "",
    isLoading: true,
  };

  componentDidMount() {
    this.fetchDataGlobal();
  }

  fetchDataGlobal = async () => {
    try {
      const jsonPositif = await (
        await fetch("https://api.kawalcorona.com/positif")
      ).json();
      const jsonSembuh = await (
        await fetch("https://api.kawalcorona.com/sembuh")
      ).json();
      const jsonMeninggal = await (
        await fetch("https://api.kawalcorona.com/meninggal")
      ).json();

      if (jsonPositif && jsonSembuh && jsonMeninggal) {
        this.setState({ isLoading: false });
        this.setState({ positif: jsonPositif.value });
        this.setState({ sembuh: jsonSembuh.value });
        this.setState({ meninggal: jsonMeninggal.value });
      }
    } catch (err) {
      alert(err);
    }
  };

  render() {
    const { navigation } = this.props;
    const handleGoTo = (screen) => {
      navigation.navigate(screen);
    };
    if (this.state.isLoading) {
      //Loading View while data is loading
      return (
        <View style={{ flex: 1, paddingTop: 20 }}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        {/* <StatusBar /> */}
        <ScrollView>
          <View style={styles.top}>
            <View style={styles.buttonBack}>
              <TouchableOpacity
                onPress={() => {
                  navigation.goBack();
                }}
              >
                <Icon name="chevron-left" size={30} color={colors.text.dark} />
              </TouchableOpacity>
            </View>

            <Text style={styles.hi}>
              Hi, {this.props.route.params.userName}
            </Text>

            <Text style={styles.textHeader}>
              Berikut data terbaru mengenai penularan virus Covid-19 secara
              global:
            </Text>

            <View style={styles.infoContainer}>
              <Infos
                title="Positif"
                content={this.state.positif}
                color="orange"
              />
              <Infos title="Sembuh" content={this.state.sembuh} color="green" />
              <Infos
                title="Meninggal"
                content={this.state.meninggal}
                color="red"
              />
            </View>

            <View style={styles.moreContainer}>
              <TouchableOpacity
                onPress={() => {
                  handleGoTo("Global");
                }}
              >
                <View style={styles.moreView}>
                  <Text style={styles.moreText}>More</Text>
                  <Icon
                    name="arrow-right"
                    size={20}
                    style={{ marginRight: 40 }}
                  />
                </View>
              </TouchableOpacity>
            </View>
          </View>

          <Text style={styles.textMenuCaption}>Jelajahi lebih lanjut</Text>

          <View style={styles.bottom}>
            <View style={{ flexDirection: "row" }}>
              <TouchableOpacity
                onPress={() => {
                  handleGoTo("Covid");
                }}
              >
                <Menus names="Covid-19" images={Covid} />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  handleGoTo("Cegah");
                }}
              >
                <Menus names="Pencegahan" images={Cegah} pages="Cegah" />
              </TouchableOpacity>
            </View>

            <View style={{ flexDirection: "row" }}>
              <TouchableOpacity
                onPress={() => {
                  handleGoTo("Ciri");
                }}
              >
                <Menus names="Ciri-ciri" images={Ciri} />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  handleGoTo("Video");
                }}
              >
                <Menus names="Video" images={Video} />
              </TouchableOpacity>
            </View>

            <View style={{ flexDirection: "row" }}>
              <TouchableOpacity
                onPress={() => {
                  handleGoTo("Update");
                }}
              >
                <Menus names="Info" images={Info} />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  handleGoTo("About");
                }}
              >
                <Menus names="About Me" images={About} />
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  top: {
    backgroundColor: colors.default,
    height: 300,
    width: "100%",
    marginTop: 0,
    borderBottomEndRadius: 50,
    borderBottomStartRadius: 50,
  },
  buttonBack: {
    marginTop: 25,
    marginLeft: 15,
  },
  hi: {
    fontSize: 24,
    fontFamily: fonts.default,
    color: colors.text.dark,
    fontWeight: "bold",
    marginLeft: 15,
  },
  textHeader: {
    fontSize: 14,
    fontWeight: "bold",
    fontFamily: fonts.default,
    color: colors.text.dark,
    // textAlign: "center",
    marginHorizontal: 15,
  },

  infoContainer: {
    flexDirection: "row",
    alignContent: "center",
    // backgroundColor: "blue",
    alignContent: "center",
    justifyContent: "space-around",
  },

  moreContainer: {
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 15,
  },
  moreView: {
    flexDirection: "row",
    alignItems: "center",
  },
  moreText: {
    fontSize: 18,
    fontWeight: "bold",
  },

  bottom: {
    alignItems: "center",
    // backgroundColor: "blue",
  },
  textMenuCaption: {
    fontSize: 18,
    fontWeight: "bold",
    fontFamily: fonts.default,
    color: colors.text.dark,
    textAlign: "center",
    marginVertical: 20,
  },
  next: {
    textAlign: "right",
    marginRight: 20,
  },
});
