import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import { fonts } from "./../../utils/fonts";

export default class Infos extends Component {
  render() {
    return (
      <View style={[styles.infoView, { backgroundColor: this.props.color }]}>
        <Text style={styles.infoTitle}>{this.props.title}</Text>
        <Text style={styles.infoCaption}>{this.props.content}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  infoView: {
    backgroundColor: "red",
    height: 100,
    width: 130,
    marginHorizontal: 10,
    marginVertical: 10,
    justifyContent: "space-around",
    alignItems: "center",
    borderRadius: 20,
  },
  infoTitle: {
    fontWeight: "bold",
    fontSize: 14,
    fontFamily: fonts.default,
  },
  infoCaption: {
    fontSize: 24,
    fontWeight: "bold",
    fontFamily: fonts.default,
  },
});
