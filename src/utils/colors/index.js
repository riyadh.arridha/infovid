export const colors = {
  default: "#FFD32A",
  disable: "#BDBDBD",
  text: {
    dark: "#2F2E41",
    semiDark: "#4F4F4F",
    minDark: "#828282",
  },
  infoBox:{
    orange:'#F2994A',
    green:'#27Ae60',
    red:'#EB5757'
  }
};
