import React, {Component} from 'react';
import {View, Text, Image, TextInput} from 'react-native';

const SampleComponent = ()=> {
    return (
      <View>
        <View style={{width:80, height:80, backgroundColor:'#0abde3'}}/>
        <Text>Riyadh</Text>
         {/* MEMANGGIL FUNCTIONAL COMPONENT */}
        <Arridha /> 
        <Text>Polinef</Text>
         {/* MEMANGGIL FUNCTIONAL COMPONENT */}
        <Photo />
        <TextInput style={{borderWidth:1}}/>
        {/* MEMANGGIL CLASS COMPONENT */}
        <BoxGreen />
        <Profile />
      </View>
    );
  };
  
  const Arridha = ()=> {                //FUNCTIONAL COMPONENT
    return <Text>Arridha Mangku Bumi</Text>
  }
  
  const Photo = ()=> {                  //FUNCTIONAL COMPONENT
    return <Image source={{uri:'http://placeimg.com/100/100/tech'}} style={{width:100, height:100}}/>
  }
  
  class BoxGreen extends Component {    //CLASS COMPONENT
    render(){  //MEMANG BEGINI, HARUS PAKAI RENDER KALAU PAKAI CLASS COMPONENT
      return <Text>Ini komponen dari kelas</Text>
    }
  }
  
  class Profile extends Component {     //CLASS COMPONENT
    render(){
      return (
      <View>
        <Image 
          source={{uri:'http://placeimg.com/100/100/animals'}} 
          style={{width:100, height:100}}
        />
        <Text style={{color:'blue', fontSize:24}}>Ini adalah hewan</Text>
      </View>
      )
    }
  }

  export default SampleComponent;