import React, {Component, useEffect, useState} from 'react';
import {Text,View, Image} from 'react-native';

// class FlexBox extends Component {
//     constructor(props){
//         super(props);
//         console.log('===>Constructor');
//         this.state={subscriber:200}
//     }

//     componentDidMount(){
//         console.log('===>Component Did Mount');
//         setTimeout(() => {
//             this.setState({
//                 subscriber:400,
//             });
//         }, 2000)      
//     }

//     componentDidUpdate(){
//         console.log('===>Component Did Update'); //muncul kalau ada yg berubah
//     }

//     componentWillUnmount(){
//         console.log('===>Componen Will Unmount')
//     }

//     render(){
//         console.log('===>Render')
//         // console.log('Hello debuggerrrrrr')
//         return (
//             <View>
//             <View 
//                 style={{
//                     // flex: 1, 
//                     flexDirection: 'row', 
//                     backgroundColor:'grey',
//                     alignItems: 'center',
//                     justifyContent: 'space-between',
                  
//                 }}>
//                 <View style={{height: 50, width:50, backgroundColor: 'red'}} />
//                 <View style={{height: 50, width:60, backgroundColor: 'yellow'}} />
//                 <View style={{height: 50, width:70, backgroundColor: 'green'}} />
//                 <View style={{height: 50, width:80, backgroundColor: 'purple'}} />
//             </View>
//             <View style={{flexDirection:'row', justifyContent:'space-around'}}>
//                 <Text>Beranda</Text>
//                 <Text>Video</Text>
//                 <Text>Playlist</Text>
//                 <Text>Komunitas</Text>
//                 <Text>Channel</Text>
//                 <Text>Tentang</Text>
//             </View>
//             <View style={{flexDirection:'row', marginTop:20, alignItems:'center'}}>
//             <Image 
//                 source={{
//                     uri:'http://placeimg.com/100/100/people'
//                 }}
//                 style={{width:100, height:100, borderRadius:50, marginRight:14}} />
//                 <View>
//                     <Text style={{fontSize:20, fontWeight:'bold'}}>Riyadh Arridha</Text>
//                     <Text>{this.state.subscriber} ribu subscriber</Text>
//                 </View>
//             </View>
//             </View>  
//         );
//     };
// }

const FlexBox = ()=> {
    const [subscriber, setSubscriber] = useState(200)
    useEffect(() => {
        console.log('did mount');
        setTimeout(() => {
            setSubscriber(400);
        }, 2000)
        return () => {
            console.log('did update')
        };
    }, [subscriber]);
    // useEffect(() => {
    //     console.log('did update');
    //     setTimeout(() => {
    //         setSubscriber(400);
    //     }, 2000)
    // }, [subscriber]);
    return(
        <View>
        <View 
            style={{
                // flex: 1, 
                flexDirection: 'row', 
                backgroundColor:'grey',
                alignItems: 'center',
                justifyContent: 'space-between',
              
            }}>
            <View style={{height: 50, width:50, backgroundColor: 'red'}} />
            <View style={{height: 50, width:60, backgroundColor: 'yellow'}} />
            <View style={{height: 50, width:70, backgroundColor: 'green'}} />
            <View style={{height: 50, width:80, backgroundColor: 'purple'}} />
        </View>
        <View style={{flexDirection:'row', justifyContent:'space-around'}}>
            <Text>Beranda</Text>
            <Text>Video</Text>
            <Text>Playlist</Text>
            <Text>Komunitas</Text>
            <Text>Channel</Text>
            <Text>Tentang</Text>
        </View>
        <View style={{flexDirection:'row', marginTop:20, alignItems:'center'}}>
        <Image 
            source={{
                uri:'http://placeimg.com/100/100/people'
            }}
            style={{width:100, height:100, borderRadius:50, marginRight:14}} />
            <View>
                <Text style={{fontSize:20, fontWeight:'bold'}}>Riyadh Arridha</Text>
                <Text>{subscriber} ribu subscriber</Text>
            </View>
        </View>
        </View>
    )
}

export default FlexBox; 

           {/* // <View style={{flex:1}}>
            //     <Text>Flex Box</Text>
            //     <View style={{backgroundColor:'red', flex:1, height:100}}></View>
            //     <View style={{backgroundColor:'yellow', flex:2, height:100}}></View>
            //     <View style={{backgroundColor:'green', flex:3, height:100}}></View>
            //     <View style={{backgroundColor:'blue', flex:1, height:100}}></View>
            // </View> */}
            
            {/* <View style={{flex: 1, flexDirection:'column'}}>
                <View style={{width: 50, backgroundColor: 'red'}} />
                <View style={{width: 50, backgroundColor: 'yellow'}} />
                <View style={{width: 50, backgroundColor: 'green'}} />
                <View style={{width: 50, backgroundColor: 'purple'}} />
            </View> */}
            
            {/* <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{width: 50, height: 50, backgroundColor: 'red'}} />
                <View style={{width: 50, height: 50, backgroundColor: 'yellow'}} />
                <View style={{width: 50, height: 50, backgroundColor: 'green'}} />
                <View style={{width: 50, height: 50, backgroundColor: 'purple'}} />
            </View>
            <View style={{flex: 2, flexDirection: 'row'}}>
                      <View style={{width: 50, height: 50, backgroundColor: 'purple'}} />
                      <View style={{width: 50, height: 50, backgroundColor: 'green'}} />
                      <View style={{width: 50, height: 50, backgroundColor: 'yellow'}} />
                      <View style={{width: 50, height: 50, backgroundColor: 'red'}} />
            </View> */}