// ARROW FUNCTION (FUNCTIONAL COMPONENT)
import React, { useState, useEffect } from "react";
import { View, ScrollView } from "react-native";
// import SampleComponent from "./sampleComponent";
// import StylingReactNativeComponent from "./stylingReactNativeComponent";
// import FlexBox from "./FlexBox";
// import Position from "./Position";

const App = () => {
  // const [isShow, SetIsShow] = useState(true);
  // useEffect(() => {
  //   setTimeout(() => {
  //     SetIsShow(false);
  //   }, 6000);
  // }, []);
  return (
    <View>
      {/* <ScrollView /> */}
      {/* <SampleComponent />
        <StylingReactNativeComponent /> */}
      {/* {isShow && <FlexBox />} */}
      {/* <FlexBox /> */}
      {/* <Position /> */}
      {/* </ScrollView> */}
    </View>
  );
};

export default App;
